package com.niuniu.rankings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuCustomScoreDetail {
  private Map<String,Float> score_detail;
  private float total_score;
  
  public NiuniuCustomScoreDetail(){
    score_detail = new HashMap<String, Float>();
    total_score = 0;
  }
  
  public void addScore(String name, float score){
    if(score_detail.containsKey(name))
      return;
    score_detail.put(name, score);
    total_score += score;
  }
  
  public float getTotalScore(){
    return total_score;
  }
  
  public Iterator<Map.Entry<String,Float>> getScoreDetailIterator(){
    return score_detail.entrySet().iterator();
  }
  
  public void reset(){
    score_detail.clear();
    total_score = 0;
  }
  
  public String toString(){
    String res = "";
    Iterator<Map.Entry<String, Float>> entries = score_detail.entrySet().iterator();
    while(entries.hasNext()){
      Map.Entry entry = (Map.Entry) entries.next();
      String reason = (String)entry.getKey();
      String score  = Float.toString((Float)entry.getValue());
      if(entries.hasNext())
        res += reason + "(" + score + ")\r\n + ";
      else
        res += reason + "(" + score + ")\r\n";
    }
    return res;
  }

}
