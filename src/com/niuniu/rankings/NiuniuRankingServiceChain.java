package com.niuniu.rankings;

import java.util.ArrayList;
import java.util.List;
import org.apache.solr.core.PluginInfo;
import org.apache.solr.core.SolrCore;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.update.processor.UpdateRequestProcessorChain;
import org.apache.solr.util.plugin.PluginInfoInitialized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuRankingServiceChain implements PluginInfoInitialized{
  
  public final static Logger log = LoggerFactory.getLogger(UpdateRequestProcessorChain.class);

  private List<NiuniuBaseFeatureFactory> factoryChain;
  private List<NiuniuBaseFeature> chain;
  private List<Float> weights;
  private SolrQueryRequest req;
  
  public List<NiuniuBaseFeature> getChain() {
    return chain;
  }
  
  public List<Float> getWeights() {
    return weights;
  }

  private final SolrCore solrCore;
  
  public String getUserInfo() {
    return userInfo;
  }

  public void setUserInfo(String userInfo) {
    this.userInfo = userInfo;
  }

  private String userInfo;
  
  public NiuniuRankingServiceChain(List<NiuniuBaseFeatureFactory> factoryChain, List<Float> weights, SolrCore solrCore) {
    this.factoryChain = factoryChain;
    this.solrCore = solrCore;
    this.weights = weights;
  }
  
  public NiuniuRankingServiceChain(List<NiuniuBaseFeatureFactory> factoryChain, List<Float> weights, SolrCore solrCore, SolrQueryRequest req) {
    this.factoryChain = factoryChain;
    this.solrCore = solrCore;
    this.weights = weights;
    this.req = req;
  }
  
  public List<NiuniuBaseFeatureFactory> getFactoryChain(){
    return this.factoryChain;
  }
  
  public SolrCore getSolrCore(){
    return this.solrCore;
  }
  
  
  
  public void init(PluginInfo info){
  }
  
  public void prepare(){
    this.chain = new ArrayList<NiuniuBaseFeature>();
    for(int i=0;i<factoryChain.size();i++){
      NiuniuBaseFeature f = factoryChain.get(i).getInstance(userInfo);
      f.prepare(req);
      //chain is null
      chain.add(f);
    }
  }
  
  public float score(int docID){
    NiuniuCustomScoreDetail niuniuCustomScoreDetail = new NiuniuCustomScoreDetail();
    for(int i=0;i<chain.size();i++){
      niuniuCustomScoreDetail.addScore(chain.get(i).getName(), weights.get(i) * (float)(chain.get(i).score(docID)));
    }
    
    /*
    ExecutorService service = Executors.newCachedThreadPool();
    final ExecutionHelper<Integer> runner = new ExecutionHelper<Integer>(service);
    final Lock lock = new ReentrantLock();
    niuniuCustomScoreDetail.reset();
    for(int i=0;i<chain.size();i++){
      runner.submit(new NiuniuFeatureCallable(lock, chain.get(i), niuniuCustomScoreDetail, docID));
    }
    for(final Integer i : runner){
    }
    */
    
    return niuniuCustomScoreDetail.getTotalScore();
  }
  
}
