/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.niuniu.rankings;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.util.ThreadInterruptedException;
import org.apache.solr.common.params.*;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.ResultContext;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.search.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;

/**
 * TODO!
 * 
 *
 * @since solr 1.3
 */
public class NiuniuCustomComponent extends SearchComponent {
	public static final String COMPONENT_NAME = "niuniu_custom";
	public final static Logger log = LoggerFactory.getLogger(NiuniuCustomComponent.class);

	@Override
	public void prepare(ResponseBuilder rb) throws IOException {
		SolrQueryRequest req = rb.req;
		SolrParams params = req.getParams();
	}

	public void reScore(ResponseBuilder rb, List<Integer> arr_id, List<Float> arr_score,
			NiuniuRankingServiceChain processorChain) {
		DocList res = rb.getResults().docList;
		DocIterator iter = res.iterator();
		try {
			while (iter.hasNext()) {
				int tmp_id = iter.nextDoc();
				float origin_score = iter.score();
				arr_id.add(tmp_id);
				arr_score.add(origin_score + processorChain.score(tmp_id));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void fillResults(ResponseBuilder rb, List<Integer> arr_id, List<Float> arr_score) {
		DocList res = rb.getResults().docList;
		DocIterator iter = res.iterator();
		while (iter.hasNext()) {
			int tmp_id = iter.nextDoc();
			float origin_score = iter.score();
			arr_id.add(tmp_id);
			arr_score.add(origin_score);
		}
	}

	public void process(ResponseBuilder rb) throws IOException {
		SolrQueryRequest req = rb.req;
		SolrParams params = req.getParams();
		SolrQueryResponse rsp = rb.rsp;
		DocList res = rb.getResults().docList;
		AtomicReader atomicReader = rb.req.getSearcher().getAtomicReader();
		int offset = params.getInt("start", 0);
		int limit = params.getInt("rows", 20);
		boolean resort = params.getBool("resort", false);
		String sort_chain = params.get("sort_chain", "");
		String chaos_field = params.get("chaos_field", "user_id");
		String chaos = params.get("chaos", "0");

		List<Integer> arr_id = new ArrayList<Integer>();
		List<Float> arr_score = new ArrayList<Float>();

		if (!params.getBool(COMPONENT_NAME, true)) {
			return;
		}

		if (resort && sort_chain != null && !sort_chain.isEmpty()) {
			NiuniuRankingServiceChain processorChain = req.getCore().getRankingServiceChain(sort_chain);
			processorChain = new NiuniuRankingServiceChain(processorChain.getFactoryChain(),
					processorChain.getWeights(), processorChain.getSolrCore(), req);
			processorChain.setUserInfo(req.getParams().get("userinfo"));
			processorChain.prepare();
			reScore(rb, arr_id, arr_score, processorChain);
			reSort(arr_id, arr_score);
		} else {
			// put result ids and scores into
			fillResults(rb, arr_id, arr_score);
		}

		// 后处理，例如打散逻辑等
		if (!chaos.equals("0")) {
			PostRanker niuniuPostRanker = new PostRanker(Math.min(limit + offset, res.matches()));
			niuniuPostRanker.process(chaos_field, chaos, arr_id, arr_score, atomicReader);
		}

		int[] ids = new int[arr_id.size()];
		ids = Ints.toArray(arr_id);
		float[] scores = new float[arr_score.size()];
		scores = Floats.toArray(arr_score);

		SolrIndexSearcher.QueryCommand cmd = rb.getQueryCommand();
		DocSlice new_res = new DocSlice(res.offset(), Math.min(res.matches(), ids.length), ids, scores, res.matches(),
				res.maxScore());
		DocListAndSet docListAndSet = new DocListAndSet();
		docListAndSet.docList = new_res.subset(cmd.getOffset(), cmd.getLen(), true);
		rb.setResults(docListAndSet);

		ResultContext ctx = new ResultContext();
		ctx.docs = rb.getResults().docList;
		ctx.query = rb.getQuery();
		rsp.add("response", ctx);
		rsp.getToLog().add("hits", rb.getResults().docList.matches());
	}

	public class NiuniuDocument {
		public int id;
		public float score;

		public NiuniuDocument(int id, float score) {
			this.id = id;
			this.score = score;
		}
	}

	// 这里需要稳定的排序，不能快排,所以用Collection.sort
	private void reSort(List<Integer> ids, List<Float> scores) {
		assert scores != null;
		assert ids.size() == scores.size();
		ArrayList<NiuniuDocument> res = new ArrayList<NiuniuDocument>();
		for (int i = 0; i < ids.size(); i++) {
			res.add(new NiuniuDocument(ids.get(i), scores.get(i)));
		}

		Collections.sort(res, new Comparator<NiuniuDocument>() {
			public int compare(NiuniuDocument o1, NiuniuDocument o2) {
				if (o2.score - o1.score > 0)
					return 1;
				if (o2.score - o1.score == 0)
					return 0;
				return -1;
			}
		});
		ids.clear();
		scores.clear();
		for (NiuniuDocument ele : res) {
			ids.add(ele.id);
			scores.add(ele.score);
		}
	}

	// 打散等最终处理逻辑
	private class PostRanker {

		private int requestedLen;

		public PostRanker(int requestedLen) {
			this.requestedLen = requestedLen;
		}

		private boolean isQualified(List<NumericDocValues> chaos_infos, int doc_id,
				List<HashMap<Long, Integer>> counters, int[] c_size) {
			for (int i = 0; i < chaos_infos.size(); i++) {
				long value = chaos_infos.get(i).get(doc_id);
				HashMap<Long, Integer> counter = counters.get(i);
				if(value==0)//该文档在这个字段的值为空，不进行chaos处理
			    	continue;
				int cnt = counter.containsKey(value) ? counter.get(value) : 0;
				if (cnt >= c_size[i]) {
					return false;
				}
			}
			return true;
		}

		private void updateCounters(List<HashMap<Long, Integer>> counters, int docId,
				List<NumericDocValues> chaos_infos) {
			for (int i = 0; i < counters.size(); i++) {
				HashMap<Long, Integer> dict = counters.get(i);
				long value = chaos_infos.get(i).get(docId);
				if (value == 0)//没有值的字段不进行chaos处理，所以不update
					continue;
				if (dict.containsKey(value))
					dict.put(value, dict.get(value) + 1);
				else {
					dict.put(value, 1);
				}
			}
		}

		public void process(String chaos_field, String chaos_size, List<Integer> arr_id, List<Float> arr_score,
				AtomicReader atomicReader) {
			if (chaos_size.equals("0"))
				return;
			String[] c_fields = chaos_field.split(",");
			String[] c_value = chaos_size.split(",");
			if (c_fields.length != c_value.length)
				return;
			int[] c_size = new int[c_value.length];

			for (int i = 0; i < c_size.length; i++) {
				c_fields[i] = c_fields[i].trim();
				c_size[i] = NumberUtils.toInt(c_value[i]);
				if (c_size[i] < 0)
					c_size[i] *= -1;
				if (c_size[i] == 0)
					return;
			}

			List<HashMap<Long, Integer>> counters = new ArrayList<HashMap<Long, Integer>>();
			for (int i = 0; i < c_size.length; i++)
				counters.add(new HashMap<Long, Integer>());
			List<Integer> id1 = new ArrayList<Integer>();
			List<Float> score1 = new ArrayList<Float>();

			try {
				List<Integer> id_array = new ArrayList<Integer>();
				List<Float> score_array = new ArrayList<Float>();
				List<NumericDocValues> chaos_infos = new ArrayList<NumericDocValues>();
				for (String str : c_fields) {
					chaos_infos.add(atomicReader.getNumericDocValues(str));
				}

				int idx = 0;
				for (idx = 0; idx < arr_id.size(); idx++) {
					// Document document = doc(arr_id.get(i));
					// user_id = document.get("user_id");
					int docId = arr_id.get(idx);
					if (isQualified(chaos_infos, docId, counters, c_size)) {
						id1.add(docId);
						updateCounters(counters, docId, chaos_infos);
						if (arr_score != null)
							score1.add(arr_score.get(idx));
						if (id1.size() >= requestedLen) {
							idx += 1;
							break;
						}
					} else {
						id_array.add(docId);
						score_array.add(arr_score.get(idx));
					}
				}
				id1.addAll(id_array);
				id1.addAll(arr_id.subList(idx, arr_id.size()));
				if (arr_score != null) {
					score1.addAll(score_array);
					score1.addAll(arr_score.subList(idx, arr_score.size()));
				}
				arr_id.clear();
				arr_score.clear();
				arr_id.addAll(id1);
				arr_score.addAll(score1);
				// arr_id = id1;
				// arr_score = score1;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/////////////////////////////////////////////
	/// SolrInfoMBean
	////////////////////////////////////////////

	public String getDescription() {
		return "query";
	}

	@Override
	public String getSource() {
		return "$URL: https://svn.apache.org/repos/asf/lucene/dev/branches/branch_4x/solr/core/src/java/org/apache/solr/handler/component/NiuniuComponent.java $";
	}

	public static class ExecutionHelper<T> implements Iterator<T>, Iterable<T> {
		private final CompletionService<T> service;
		private int numTasks;

		ExecutionHelper(final Executor executor) {
			this.service = new ExecutorCompletionService<T>(executor);
		}

		@Override
		public boolean hasNext() {
			return numTasks > 0;
		}

		public void submit(Callable<T> task) {
			this.service.submit(task);
			++numTasks;
		}

		@Override
		public T next() {
			if (!this.hasNext())
				throw new NoSuchElementException("next() is called but hasNext() returned false");
			try {
				return service.take().get();
			} catch (InterruptedException e) {
				throw new ThreadInterruptedException(e);
			} catch (ExecutionException e) {
				throw new RuntimeException(e);
			} finally {
				--numTasks;
			}
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Iterator<T> iterator() {
			// use the shortcut here - this is only used in a private context
			return this;
		}
	}

}
