package com.niuniu.rankings;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class NiuniuFeatureCallable  implements Callable<Integer>{
  NiuniuBaseFeature feature;
  NiuniuCustomScoreDetail scoreDetail;
  private Lock lock;
  int docId;
  
  public NiuniuFeatureCallable(Lock lock, NiuniuBaseFeature feature, NiuniuCustomScoreDetail scoreDetail, int docId) {
    this.lock = lock;
    this.feature = feature;
    this.scoreDetail = scoreDetail;
    this.docId = docId;
  }
  
  public Integer call() throws Exception {
    try{
      int score = feature.score(docId);
      lock.lock();
      scoreDetail.addScore(feature.getName(), score);
    }finally{
      lock.unlock();
    }
    return 0;
  }
  
}
