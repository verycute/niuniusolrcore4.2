/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.niuniu.component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.FlagsAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PayloadAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.spelling.*;
import org.apache.solr.util.plugin.SolrCoreAware;

public class NiuniuRealtimeTaggingComponent extends SearchComponent implements SolrCoreAware{
	private static final Logger LOG = LoggerFactory.getLogger(NiuniuRealtimeTaggingComponent.class);

	public static final boolean DEFAULT_ONLY_MORE_POPULAR = false;

	/**
	 * Base name for all spell checker query parameters. This name is also used
	 * to register this component with SearchHandler.
	 */
	public static final String COMPONENT_NAME = "realtime_tagging";

	@SuppressWarnings("unchecked")
	protected NamedList initParams;

	/**
	 * Key is the dictionary, value is the SpellChecker for that dictionary name
	 */

	protected QueryConverter queryConverter;
	protected Analyzer analyzer;
	protected String field_name;
	boolean detail;

	Map<String, Integer> tagWeightForBrand = null;
	String defaultField = null;
	Set<String> brand_set = null;
	Map<String, String> model_map = null;
	Map<String, String> price_map = null;
	String brand_file;
	String model_file;
	String price_file;
	int maxTagCount = 0;

	public void init(NamedList args) {
		super.init(args);
		this.initParams = args;
		defaultField = args.get("default_field").toString();
		String[] tags = args.get("tags").toString().split(",");
		String[] weights = args.get("weights").toString().split(",");
		maxTagCount = NumberUtils.toInt(args.get("max_tag").toString().trim());
		if (tags.length == weights.length) {
			tagWeightForBrand = new HashMap<String, Integer>();
			for (int i = 0; i < tags.length; i++) {
				tagWeightForBrand.put(tags[i].trim(), NumberUtils.toInt(weights[i].trim()));
			}
		}
		brand_set = new HashSet<String>();
		model_map = new HashMap<String, String>();
		price_map = new HashMap<String, String>();
		this.brand_file = args.get("brand").toString();
		this.model_file = args.get("model").toString();
		this.price_file = args.get("price").toString();
	};

	private void readBrandFile(ResourceLoader loader, String filename) {
		if (filename == null) {
			return;
		}
		InputStream is = null;
		try {
			is = loader.openResource(filename);
			if (is == null) {
				throw new RuntimeException(filename + " Dictionary not found!!!");
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 512);
			String line = null;
			brand_set.clear();
			do {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim().toLowerCase();
				brand_set.add(line);
			} while (line != null);
		} catch (IOException ioe) {
			System.err.println("brand file loading exception.");
			ioe.printStackTrace();
		}
	}

	private void readMappingFromFile(ResourceLoader loader, String filename, Map<String, String> map) {
		if (filename == null) {
			return;
		}
		InputStream is = null;
		try {
			is = loader.openResource(filename);
			if (is == null) {
				throw new RuntimeException(filename + " Dictionary not found!!!");
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"), 512);
			String line = null;
			map.clear();
			do {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim().toLowerCase();
				String[] arrs = line.split("\t");
				map.put(arrs[0], arrs[1]);
			} while (line != null);
		} catch (IOException ioe) {
			System.err.println(filename + " file loading exception.");
			ioe.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void prepare(ResponseBuilder rb) throws IOException {
		SolrParams params = rb.req.getParams();
		field_name = params.get("field");

		analyzer = rb.req.getCore().getSchema().getFieldType(field_name).getQueryAnalyzer();// .getQueryAnalyzer();
		detail = params.getBool("detail", false);
	}

	public void addToBrandIndicator(String str, int weight, Map<String, Integer> brand_indicator) {
		if (brand_indicator.containsKey(str)) {
			brand_indicator.put(str, brand_indicator.get(str) + weight);
		} else {
			brand_indicator.put(str, weight);
		}
	}

	private void getLatentBrand(String term, String type, Map<String, Integer> brand_indicator) {
		if (type.equals("BRAND")) {
			if (brand_set.contains(term)) {
				addToBrandIndicator(term, this.tagWeightForBrand.get("brand"), brand_indicator);
			}
		}
		if (type.contains("MODEL")) {
			if (model_map.containsKey(term)) {
				String[] brands_list = model_map.get(term).split(",");
				for (String s : brands_list) {
					addToBrandIndicator(s, this.tagWeightForBrand.get("model"), brand_indicator);
				}
			}
		}

	}

	private void getLatentBrandFromPrice(String term, String type, Map<String, Integer> brand_indicator) {
		if (type.contains("PRICE")) {
			if (term.length() < 3)
				return;
			if (price_map.containsKey(term)) {
				String[] brands_list = price_map.get(term).split(",");
				for (String s : brands_list) {
					if (brand_indicator.containsKey(s))
						addToBrandIndicator(s, this.tagWeightForBrand.get("price"), brand_indicator);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void process(ResponseBuilder rb) throws IOException {
		SolrParams params = rb.req.getParams();
		String q = params.get(CommonParams.Q);
		Collection<Token> tokens = null;

		if (q == null) {
			// we have a spell check param, tokenize it with the query analyzer
			// applicable for this spellchecker
			q = rb.getQueryString();
			if (q == null) {
				q = params.get(CommonParams.Q);
			}
		}
		q = q.replaceAll("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", "*");
		tokens = getTokens(q, analyzer);
		Map<String, Integer> brand_indicator = new HashMap<String, Integer>();
		for (Token token : tokens) {
			String term = token.toString();
			String type = token.type();
			if (type.equals("BRAND") || type.contains("MODEL")) {
				getLatentBrand(term, type, brand_indicator);
			}
		}
		for (Token token : tokens) {
			int start = token.startOffset();
			String term = token.toString();
			String type = token.type();
			if (type.contains("PRICE")) {
				if ((start >= 1 && q.charAt(start - 1) == '下')
						|| (start >= 2 && q.substring(start - 2, start).equals("⬇️"))) {
					continue;
				}
				getLatentBrandFromPrice(term, type, brand_indicator);
			}
		}
		List<Map.Entry<String, Integer>> infoIds = new ArrayList<Map.Entry<String, Integer>>(
				brand_indicator.entrySet());
		if (!brand_indicator.isEmpty()) {
			Collections.sort(infoIds, new Comparator<Map.Entry<String, Integer>>() {
				public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
					return (o2.getValue().compareTo(o1.getValue()));
				}
			});
		}
		int kBest = selectKBest(infoIds);

		NamedList results = toNamedList(tokens, q, kBest, infoIds);
		rb.rsp.add("results", results);
	}

	private Collection<Token> getTokens(String q, Analyzer analyzer) throws IOException {
		Collection<Token> result = new ArrayList<Token>();
		assert analyzer != null;
		TokenStream ts = analyzer.tokenStream(field_name, new StringReader(q));
		ts.reset();
		// TODO: support custom attributes
		CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
		OffsetAttribute offsetAtt = ts.addAttribute(OffsetAttribute.class);
		TypeAttribute typeAtt = ts.addAttribute(TypeAttribute.class);
		FlagsAttribute flagsAtt = ts.addAttribute(FlagsAttribute.class);
		PayloadAttribute payloadAtt = ts.addAttribute(PayloadAttribute.class);
		PositionIncrementAttribute posIncAtt = ts.addAttribute(PositionIncrementAttribute.class);

		while (ts.incrementToken()) {
			Token token = new Token();
			token.copyBuffer(termAtt.buffer(), 0, termAtt.length());
			token.setOffset(offsetAtt.startOffset(), offsetAtt.endOffset());
			token.setType(typeAtt.type());
			token.setFlags(flagsAtt.getFlags());
			token.setPayload(payloadAtt.getPayload());
			token.setPositionIncrement(posIncAtt.getPositionIncrement());
			result.add(token);
		}
		ts.end();
		ts.close();
		return result;
	}

	private int selectKBest(List<Map.Entry<String, Integer>> infoIds) {
		if (infoIds == null || infoIds.size() == 0)
			return 0;
		if (infoIds.size() == 1)
			return 1;
		int reserve = infoIds.get(0).getValue();
		int top = 0;
		for (int i = 1; i < infoIds.size() && i < maxTagCount; i++) {
			int tmp = infoIds.get(i).getValue();
			if ((tmp + tmp / 2) >= reserve || tmp >= 2 * tagWeightForBrand.get("brand")) {
				reserve = tmp;
				top++;
			} else {
				break;
			}
		}
		if (top == infoIds.size())
			top--;
		return ++top;
	}

	protected NamedList toNamedList(Collection<Token> tokens, String origQuery, int kBest, List infoIds) {
		SimpleOrderedMap tokenList = new SimpleOrderedMap();
		tokenList.add("query", origQuery);
		String result = "";
		if (infoIds.size() == 0) {
			tokenList.add("brand_tagging", "");
			return tokenList;
		}
		if (detail) {
			for (int i = 0; i < kBest && i < infoIds.size(); i++) {
				Map.Entry<String, Integer> cur = (Map.Entry<String, Integer>) infoIds.get(i);
				result = result + cur.getKey() + "||" + cur.getValue() + " ";
			}
		} else {
			for (int i = 0; i < kBest && i < infoIds.size(); i++) {
				Map.Entry<String, Integer> cur = (Map.Entry<String, Integer>) infoIds.get(i);
				result = result + cur.getKey() + " ";
			}
		}

		tokenList.add("brand_tagging", result.trim());
		return tokenList;
	}

	@Override
	public String getDescription() {
		return "A Realtime Tagging component";
	}

	@Override
	public String getSource() {
		return "Niuniu Realtime Tagging Component";
	}

	@Override
	public void inform(SolrCore core) {
		readBrandFile(core.getResourceLoader(), brand_file);
		readMappingFromFile(core.getResourceLoader(), model_file, model_map);
		readMappingFromFile(core.getResourceLoader(), price_file, price_map);
	}
}